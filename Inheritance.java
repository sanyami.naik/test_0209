package test_0209;

class Bank{
    public void rateOfInterest()
    {
        System.out.println("The general Rate of interest method ");
    }
}

class SBI extends Bank
{
    public void rateOfInterest()
    {
        System.out.println("The SBI Rate of interest method ");
        System.out.println("THe rate of interest is 4%");

    }
}

class HDFC extends Bank{
    public void rateOfInterest()
    {
        System.out.println("The HDFC Rate of interest method ");
        System.out.println("THe rate of interest is 8%");

    }
}

public class Inheritance {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.rateOfInterest();

        SBI sbi=new SBI();
        sbi.rateOfInterest();

        HDFC hdfc=new HDFC();
        hdfc.rateOfInterest();

    }
}


/*

OUTPUT
The general Rate of interest method
The SBI Rate of interest method
THe rate of interest is 4%
The HDFC Rate of interest method
THe rate of interest is 8%

 */