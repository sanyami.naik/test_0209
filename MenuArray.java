package test_0209;


import java.util.Arrays;
import java.util.Scanner;

class PerformArrayOperations
{

    Scanner sc =new Scanner(System.in);
    //static int numberOfElements;
    int[] array=new int[10];
    int[] createArray(int a,int[] array)
    {
        System.out.println("Enter the elements you want");
        for(int i=0;i<a;i++)
        {
            array[i]=sc.nextInt();
        }
        return array;

    }


    void displayArray(int[] arrayy)
    {
        System.out.println("The elements of the array are");

        for(int i=0;i< arrayy.length;i++)
        {
            System.out.print(arrayy[i]+" ");
        }
        System.out.println();
    }

    void sortAndReverse(int[] arrayy)
    {
        Arrays.sort(arrayy);
        System.out.println("The elements of the array in sorted way are");

        for(int i=0;i< arrayy.length;i++)
        {
            System.out.print(arrayy[i]+" ");
        }

        System.out.println();
        System.out.println("The elements of the array in reverse way are");

        for(int i=arrayy.length-1;i>=0;i--)
        {
            System.out.print(arrayy[i]+" ");
        }
        System.out.println();

    }

}


public class MenuArray {

    public static void main(String[] args) {


    Scanner sc = new Scanner(System.in);
    PerformArrayOperations p = new PerformArrayOperations();
    System.out.println("Enter the number of elements you want");
    int numberOfElements=sc.nextInt();
    System.out.println("Enter the number of elements you want to store");
    int[] array=new int [numberOfElements];
    int[] arrayy=new int[numberOfElements];
    char ch = 'Y';

    do

    {
        System.out.println("Following are the options you have");
        System.out.println("Press 1 for creating array and storing elements");
        System.out.println("Press 2 for displaying the array elements");
        System.out.println("Press 3 for sorting and reversing the array");
        int op = sc.nextInt();

        switch (op) {
            case 1:
                arrayy=p.createArray(numberOfElements,array);
                break;

            case 2:
                p.displayArray(arrayy);
                break;

            case 3:
                p.sortAndReverse(arrayy);
                break;

            default:
                System.out.println("Plz enter a valid choice");
        }

        System.out.println("Do you want to continue? Press 'Y' for continuing");
        ch = sc.next().charAt(0);

    }while(ch=='Y'||ch=='y');

  }

}


/*
OUTPUT
Enter the number of elements you want
5
Enter the number of elements you want to store
Following are the options you have
Press 1 for creating array and storing elements
Press 2 for displaying the array elements
Press 3 for sorting and reversing the array
1
Enter the elements you want
3
4
1
87
45
Do you want to continue? Press 'Y' for continuing
y
Following are the options you have
Press 1 for creating array and storing elements
Press 2 for displaying the array elements
Press 3 for sorting and reversing the array
2
The elements of the array are
3 4 1 87 45
Do you want to continue? Press 'Y' for continuing
y
Following are the options you have
Press 1 for creating array and storing elements
Press 2 for displaying the array elements
Press 3 for sorting and reversing the array
3
The elements of the array in sorted way are
1 3 4 45 87
The elements of the array in reverse way are
87 45 4 3 1
Do you want to continue? Press 'Y' for continuing
r


 */